import 'dart:io';

List token(String ex) {
  List s = ["*", "/", "%", "^", "(", ")", "+", "-"];
  List num = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];

  ex = ex.trim();

  final _whitespaceRE = RegExp(r"\s+");
  ex = ex.split(_whitespaceRE).join(" ");

  s.forEach((i) {
    s.forEach((j) {
      ex = ex.replaceAll("$i$j", "$i $j");
    });
  });

  num.forEach((i) {
    s.forEach((j) {
      ex = ex.replaceAll("$j$i", "$j $i");
      ex = ex.replaceAll("$i$j", "$i $j");
    });
  });
}

int checkPrecedenceLevel(String operator) {
  if (operator == "(" || operator == ")") {
    return 4;
  }
  if (operator == "^") {
    return 3;
  }
  if (operator == "*" || operator == "/") {
    return 2;
  }
  if (operator == "%") {
    return 3;
  }
  if (operator == "+" || operator == "-") {
    return 1;
  }
  return 0;
}

bool isInteger(String str) {
  if (str == null) {
    return false;
  }
  return double.tryParse(str) != null;
}





void main(List<String> arguments) {
  print('Input infix :');
  String ex = stdin.readLineSync() as String;
 
 
}
